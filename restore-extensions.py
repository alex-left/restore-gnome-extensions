#!/usr/bin/env python3

import subprocess
from os import path
import logging

def load_yaml(path):
    """
    Read and load YAML config file.

    :param path: filepath of yaml configfile
    :type path: str
    :return: yaml content
    :rtype: dict
    """
    try:
        import yaml
    except ImportError as e:
        logging.error("""A error ocurred when trying to use the pyyaml library.
              This library must be installed to use this program.
              You can try with 'pip install pyyaml' command {}""".format(e))
        raise SystemExit
    try:
        with open(path, 'r') as file:
            config = yaml.safe_load(file)
        return config
    except Exception as e:
        raise(e)

def main():
    config = load_yaml(path.dirname(__file__) + '/config.yml')
    extensions_to_enable = sorted(config["extensions"])
    for ext in extensions_to_enable:
        res = subprocess.run(["gnome-shell-extension-tool", "-e", ext])

if __name__ == '__main__':
    main()
